/****************************************************************************
**  localsocket.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "localsocket.h"
#include "protocol/irggu.h"
#include <QStringListIterator>
#include <QRegularExpression>

LocalSocket::LocalSocket (QObject *parent) : QObject(parent)
{
    socket  = new QLocalSocket(this);

    connect(socket, SIGNAL(connected()), this, SLOT(sendHandshake()));
    connect(socket, SIGNAL(disconnected()), this, SIGNAL(disconnected()));
    connect(socket, SIGNAL(error(QLocalSocket::LocalSocketError)), this,
            SIGNAL(error(QLocalSocket::LocalSocketError)));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
}

void LocalSocket::connectToServer (const QString &name)
{
    socket->connectToServer(name);
}

void LocalSocket::disconnectFromServer ()
{
    socket->disconnectFromServer();
}

bool LocalSocket::canReadLine ()
{
    return socket->canReadLine();
}

QByteArray LocalSocket::readLine ()
{
    return socket->readLine();
}

qint64 LocalSocket::write (const char *data)
{
    return socket->write(data);
}

void LocalSocket::handleHandshake (QString line)
{
    QRegularExpression re(IrGGu_IRC_Handshake::receive);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        receivedHandshake = true;

        if ( rem.captured("version").toFloat() == IrGGu_IRC::version )
        {
            Q_EMIT connected();
        }
    }
}

/**
 * Read data sent by client.
 */
void LocalSocket::readData ()
{
    if ( !receivedHandshake )
    {
        QString line;

        while ( socket->canReadLine() )
        {
            line = QString::fromUtf8(socket->readLine()).trimmed();

            handleHandshake(line);
        }
    }
    else
    {
        Q_EMIT readyRead();
    }
}

void LocalSocket::sendHandshake ()
{
    receivedHandshake = false;

    QString version = QString::number(IrGGu_IRC::version, 10, 1);

    QString line = IrGGu_IRC_Handshake::write + "\n";

    line.replace("<version>", version);

    socket->write(line.toUtf8());
}
