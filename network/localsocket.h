/****************************************************************************
**  localsocket.h
**
**  Copyright information
**
**      Copyright (C) 2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef LOCALSOCKET_H
#define LOCALSOCKET_H

#include <QObject>
#include <QtNetwork/QLocalSocket>

class LocalSocket : public QObject
{
    Q_OBJECT
public:
    explicit LocalSocket(QObject *parent = 0);
    
    void       connectToServer(const QString &name);
    void       disconnectFromServer();
    bool       canReadLine();
    QByteArray readLine();
    qint64     write(const char *data);

private:
    QLocalSocket *socket;
    bool         receivedHandshake;

    void handleHandshake(QString line);

private Q_SLOTS:
    void readData();
    void sendHandshake();

Q_SIGNALS:
    void connected();
    void disconnected();
    void error(QLocalSocket::LocalSocketError);
    void readyRead();
    
};

#endif // LOCALSOCKET_H
