/****************************************************************************
**  handler_server.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "handler_server.h"
#include "connection.h"
#include "protocol/irggu.h"
#include <QCryptographicHash>
#include <QDebug>
#include <QRegExp>
#include <QTextCodec>
#include <QCoreApplication>
#include <QRegularExpressionMatch>

int argc   = 1;
char *argv = "/";

handleFunctionList handleFunctions;
QCoreApplication   *a;
Connection         *connection;
bool               isConnected = false;
QTextCodec         *codec      = QTextCodec::codecForLocale();

extern "C" int utf8;

extern "C" void       changeServerIrssi(WINDOW_REC *win, SERVER_REC *server);
extern "C" void       writeIrssi(char *line, SERVER_REC *server, WI_ITEM_REC *item);
extern "C" void       writeClientNotice(const char *line);
extern "C" void       registered();
extern "C" void       unregistered();
extern "C" void       saveStr(char *key, char *str);
extern "C" void       saveInt(char *key, int value);
extern "C" void       setWindowActive(WINDOW_REC *window);
extern "C" char       *getStr(char *key);
extern "C" int        getInt(char *key);
extern "C" int        getBool(char *key);
extern "C" char       *completeIrssi(char *line, int pos, WINDOW_REC *win);
extern "C" WINDOW_REC *findWindow(SERVER_REC *server, const char *name);

static void connected();
static void disconnected();
static void printMsg(QString msg);
static void clientNotice(QString line);
static void registeration(QString line);
static void newLine(QString line);
static void newCompleteLine(QString line);
static void newDccFileReply(QString line);
static void quitNetwork(QString line);
static void closeChat(QString line);
static void query(QString line);

/**
 * Write settings to server.
 */
void writeSettings ()
{
    QString command = IrGGu_IRC_Write::newSettingValue;

    command.replace("<category>", "scrollback");
    command.replace("<setting>", "scrollback");
    command.replace("<value>", QString::number(getInt("scrollback")));

    connection->write(command);

    QString timestamp;

    command = IrGGu_IRC_Write::newSettingValue;

    if ( utf8 )
    {
        timestamp = QString::fromUtf8(getStr("timestampState"));
    }
    else
    {
        timestamp = codec->toUnicode(getStr("timestampState"));
    }

    command.replace("<category>", "timestamp");
    command.replace("<setting>", "state");
    command.replace("<value>", timestamp);

    connection->write(command);

    command = IrGGu_IRC_Write::newSettingValue;

    if ( utf8 )
    {
        timestamp = QString::fromUtf8(getStr("timestampFormat"));
    }
    else
    {
        timestamp = codec->toUnicode(getStr("timestampFormat"));
    }

    command.replace("<category>", "timestamp");
    command.replace("<setting>", "format");
    command.replace("<value>", timestamp);

    connection->write(command);

    command = IrGGu_IRC_Write::setColors;
    QString colors;
    QString color;

    if ( utf8 )
    {
        colors = QString::fromUtf8(getStr("foreground"));
    }
    else
    {
        colors = codec->toUnicode(getStr("foreground"));
    }

    for ( int i = 0; i < 16; i++ )
    {
        color = "local_" + QString::number(i);

        if ( utf8 )
        {
            colors += " " + QString::fromUtf8(getStr(color.toUtf8().data())) ;
        }
        else
        {
            colors += " " + codec->toUnicode(getStr(codec->fromUnicode(color).data()));
        }
    }

    for ( int i = 0; i < 16; i++ )
    {
        color = "mirc_" + QString::number(i);

        if ( utf8 )
        {
            colors += " " + QString::fromUtf8(getStr(color.toUtf8().data()));
        }
        else
        {
            colors += " " + codec->toUnicode(getStr(codec->fromUnicode(color).data()));
        }
    }

    command.replace("<colors>", colors);

    connection->write(command);
}

/**
 * Called when connected to IrGGu-server.
 */
void connected ()
{
    isConnected = true;
    writeClientNotice("Connected to IrGGu server.");

    writeSettings();

    if ( getBool("autoregister") )
    {
        QString username;
        QString password;

        if ( utf8 )
        {
            username = QString::fromUtf8(getStr("username"));
            password = QString::fromUtf8(getStr("password"));
        }
        else
        {
            username = codec->toUnicode(getStr("username"));
            password = codec->toUnicode(getStr("password"));
        }

        if ( !username.isEmpty() && !password.isEmpty() )
        {
            writeClientNotice("Automatic registeration is enabled.");
            writeClientNotice("Registering to server.");

            QString command = IrGGu_IRC_Write::registerIRC;

            command.replace("<username>", username);
            command.replace("<password>", password);

            connection->write(command);
        }
    }
}

/**
 * Called when disconnected from IrGGu-server.
 */
void disconnected ()
{
    isConnected = false;

    serversList.clear();
    chatsList.clear();

    writeClientNotice("Disconnected from IrGGu server.");

    unregistered();
}

/**
 * Print message to irssi.
 */
void printMsg (QString msg)
{
    if ( utf8 )
    {
        writeClientNotice(msg.toUtf8());
    }
    else
    {
        writeClientNotice(codec->fromUnicode(msg));
    }
}

/**
 * Handles client notice command.
 *
 * @param line Line that contains command.
 */
void clientNotice (QString line)
{
    QRegularExpression re(IrGGu_IRC_Receive::clientNotice);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString lines = rem.captured("lines");
        QRegularExpression reLines("<line>(.+?)</line>");
        QRegularExpressionMatch remLines = reLines.match(lines);

        while ( remLines.hasMatch() )
        {
            if ( utf8 )
            {
                writeClientNotice(remLines.captured(1).toUtf8());
            }
            else
            {
                writeClientNotice(codec->fromUnicode(remLines.captured(1)));
            }

            remLines = reLines.match(line, remLines.capturedEnd());
        }
    }
}

/**
 * Handles register command.
 *
 * @param line Line that contains command.
 */
void registeration (QString line)
{
    QRegularExpression re(IrGGu_IRC_Receive::registerIRC);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        int result = rem.captured("result").toInt();

        if ( result == 0 )
        {
            writeClientNotice("Registered on IrGGu server!");
            registered();
        }
        else
        {
            switch ( result )
            {
                case 1:
                {
                    writeClientNotice("You are already registered!");

                    break;
                }

                case 2:
                {
                    writeClientNotice("Username in use!");

                    break;
                }

                case 3:
                {
                    writeClientNotice("Username or password are not valid!");

                    break;
                }

                case 4:
                {
                    writeClientNotice("Server is unable to get user ID!");

                    break;
                }

                default:
                {
                    writeClientNotice("Unknown error in registration!");

                    break;
                }
            }
        }
    }
    else
    {
        writeClientNotice("Registration failed!");
    }
}

/**
 * Handles write line command.
 *
 * @param line Line that contains command.
 */
void newLine (QString line)
{
    QRegularExpression re(IrGGu_IRC_Receive::writeLine);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString    networkName = rem.captured("network");
        SERVER_REC *server     = serversList.value(networkName, NULL);
        QByteArray text;
        WINDOW_REC *window;

        if ( utf8 )
        {
            text = rem.captured("line").toUtf8();
        }
        else
        {
            text = codec->fromUnicode(rem.captured("line"));
        }

        if ( server )
        {
            QString chatName = rem.captured("chat");

            if ( chatName == "(server)" )
            {
                setWindowActive(statusWindow);
                writeIrssi(text.data(), server, NULL);
            }
            else
            {
                QMap<QString, WI_ITEM_REC *> chats = chatsList.value(networkName);
                WI_ITEM_REC                  *item = chats.value(chatName, NULL);

                if ( item )
                {
                    QByteArray windowName;

                    if ( utf8 )
                    {
                        windowName = chatName.toUtf8();
                    }
                    else
                    {
                        windowName = codec->fromUnicode(chatName);
                    }

                    window = findWindow(server, windowName.data());

                    if ( window )
                    {
                        setWindowActive(window);
                        writeIrssi(text.data(), server, item);
                    }
                }
            }
        }
        else if ( networkName == "(none)" )
        {
            writeIrssi(text.data(), NULL, NULL);
        }
    }
}

/**
 * Handles complete line command.
 *
 * @param line Line that contains command.
 */
void newCompleteLine (QString line)
{
    QRegularExpression re(IrGGu_IRC_Receive::completeLine);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString    networkName   = rem.captured("network");
        QString    chatName      = rem.captured("chat");
        QString    completedLine = "";
        SERVER_REC *server       = serversList.value(networkName, NULL);
        int        pos           = rem.captured("pos").toInt();
        QByteArray text;

        if ( utf8 )
        {
            text = rem.captured("line").toUtf8();
        }
        else
        {
            text = codec->fromUnicode(rem.captured("line"));
        }

        text.replace("<space>", " ");

        if ( server )
        {
            if ( chatName == "(server)" )
            {
                changeServerIrssi(statusWindow, server);

                if ( utf8 )
                {
                    completedLine = QString::fromUtf8(completeIrssi(text.data(), pos, statusWindow));
                }
                else
                {
                    completedLine = codec->toUnicode(completeIrssi(text.data(), pos, statusWindow));
                }
            }
            else
            {
                QMap<QString, WI_ITEM_REC *> chats = chatsList.value(networkName);
                WI_ITEM_REC                  *item = chats.value(chatName, NULL);

                if ( item )
                {
                    WINDOW_REC *window = static_cast<WINDOW_REC *>(item->window);

                    if ( utf8 )
                    {
                        completedLine = QString::fromUtf8(completeIrssi(text.data(), pos, window));
                    }
                    else
                    {
                        completedLine = codec->toUnicode(completeIrssi(text.data(), pos, window));
                    }
                }
            }
        }
        else if ( networkName == "(none)" )
        {
            if ( utf8 )
            {
                completedLine = QString::fromUtf8(completeIrssi(text.data(), pos, statusWindow));
            }
            else
            {
                completedLine = codec->toUnicode(completeIrssi(text.data(), pos, statusWindow));
            }
        }

        if ( !completedLine.isEmpty() )
        {
            int last = completedLine.length() - 1;

            if ( completedLine.at(last) == ' ' )
            {
                completedLine.replace(last, 1, "<space>");
            }

            connection->write("LINE_COMPLETED " + networkName + " " + chatName + " "
                              + completedLine);
        }
    }
}

/**
 * Handles dcc file reply.
 *
 * @param line Line that contains dcc file reply.
 */
void newDccFileReply (QString line)
{
    QRegularExpression aRe(IrGGu_IRC_Receive::acceptDccFile);
    QRegularExpression dRe(IrGGu_IRC_Receive::doNotAcceptDccFile);
    QRegularExpressionMatch aRem = aRe.match(line);
    QRegularExpressionMatch dRem = dRe.match(line);

    if ( aRem.hasMatch() )
    {
        QString    networkName = aRem.captured("network");
        SERVER_REC *server     = serversList.value(networkName, NULL);
        QByteArray text;

        if ( server )
        {
            line = "/dcc get " + aRem.captured("sender") + " " + aRem.captured("file");

            if ( utf8 )
            {
                text = line.toUtf8();
            }
            else
            {
                text = codec->fromUnicode(line);
            }

            setWindowActive(statusWindow);
            writeIrssi(text.data(), server, NULL);
        }
    }
    else if ( dRem.hasMatch() )
    {
        QString    networkName = dRem.captured("network");
        SERVER_REC *server     = serversList.value(networkName, NULL);
        QByteArray text;

        if ( server )
        {
            line = "/dcc close get " + dRem.captured("sender") + " " + dRem.captured("file");

            if ( utf8 )
            {
                text = line.toUtf8();
            }
            else
            {
                text = codec->fromUnicode(line);
            }

            setWindowActive(statusWindow);
            writeIrssi(text.data(), server, NULL);
        }
    }
}

/**
 * Handles quit network command.
 *
 * @param line Line that contains command.
 */
void quitNetwork (QString line)
{
    QRegularExpression re(IrGGu_IRC_Receive::quitNetwork);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString    networkName = rem.captured("network");
        SERVER_REC *server     = serversList.value(networkName, NULL);
        QByteArray text;

        if ( server )
        {
            line = "/disconnect";

            if ( utf8 )
            {
                text = line.toUtf8();
            }
            else
            {
                text = codec->fromUnicode(line);
            }

            setWindowActive(statusWindow);
            writeIrssi(text.data(), server, NULL);
        }
    }
}

/**
 * Handles close chat command.
 *
 * @param line Line that contains command.
 */
void closeChat (QString line)
{
    QRegularExpression re(IrGGu_IRC_Receive::closeChat);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString    networkName = rem.captured("network");
        QString    chatName    = rem.captured("chat");
        SERVER_REC *server     = serversList.value(networkName, NULL);
        QByteArray text;
        WINDOW_REC *window;

        if ( server )
        {
            line = "/window close";

            if ( utf8 )
            {
                text = line.toUtf8();
            }
            else
            {
                text = codec->fromUnicode(line);
            }

            QMap<QString, WI_ITEM_REC *> chats = chatsList.value(networkName);
            WI_ITEM_REC                  *item = chats.value(chatName, NULL);

            if ( item )
            {
                QByteArray windowName;

                if ( utf8 )
                {
                    windowName = chatName.toUtf8();
                }
                else
                {
                    windowName = codec->fromUnicode(chatName);
                }

                window = findWindow(server, windowName.data());

                if ( window )
                {
                    setWindowActive(window);
                    writeIrssi(text.data(), server, item);
                }
            }
        }
    }
}

/**
 * Handles query command.
 *
 * @param line Line that contains command.
 */
void query (QString line)
{
    QRegularExpression re(IrGGu_IRC_Receive::query);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        QString    networkName = rem.captured("network");
        SERVER_REC *server     = serversList.value(networkName, NULL);
        QByteArray text;

        if ( server )
        {
            line = "/query " + rem.captured("nick");

            if ( utf8 )
            {
                text = line.toUtf8();
            }
            else
            {
                text = codec->fromUnicode(line);
            }

            setWindowActive(statusWindow);
            writeIrssi(text.data(), server, NULL);
        }
    }
}

/**
 * Init server connection.
 */
void init ()
{
    a          = new QCoreApplication(argc, &argv);
    connection = new Connection(a);
}

/**
 * Deinit server connection.
 */
void deinit()
{
    delete a;
}

/**
 * Connects to server.
 */
void connectToServer ()
{

    handleFunctions.insert("CLIENT_NOTICE", &clientNotice);
    handleFunctions.insert("REGISTER", &registeration);
    handleFunctions.insert("WRITE_LINE", &newLine);
    handleFunctions.insert("COMPLETE_LINE", &newCompleteLine);
    handleFunctions.insert("ACCEPT_DCC_FILE", &newDccFileReply);
    handleFunctions.insert("DO_NOT_ACCEPT_DCC_FILE", &newDccFileReply);
    handleFunctions.insert("QUIT", &quitNetwork);
    handleFunctions.insert("CLOSE_CHAT", &closeChat);
    handleFunctions.insert("QUERY", &query);

    connection->setFunctions(&connected, &disconnected, &printMsg, handleFunctions);
    connection->connectToServer();
}

/**
 * Disconnects from the server.
 */
void disconnect ()
{
    isConnected = false;
    connection->disconnect();
}

/**
 * Register the IRC-client to the server.
 *
 * @param *data Username and password.
 * @param force Force registration.
 */
void registerToServer (const char *data, int force)
{
    QString command = !force ? IrGGu_IRC_Write::registerIRC : IrGGu_IRC_Write::registerForceIRC;
    QString userPass;
    QString user;
    QString pass;

    if ( utf8 )
    {
        userPass = QString::fromUtf8(data);
    }
    else
    {
        userPass = codec->toUnicode(data);
    }

    if ( userPass.contains(" ") )
    {
        user = userPass.section(" ", 0, 0);
        pass = QString::fromUtf8(QCryptographicHash::hash(userPass.section(" ", 1, 1).toUtf8(),
                                                          QCryptographicHash::Md5).toHex());

        saveStr("username", user.toUtf8().data());
        saveStr("password", pass.toUtf8().data());

        if ( isConnected )
        {
            writeClientNotice("Registering to server.");

            command.replace("<username>", user);
            command.replace("<password>", pass);

            connection->write(command);
        }
        else
        {
            writeClientNotice("Not connected to server!");
        }
    }
    else
    {
        writeClientNotice("Not enough parameters! Give username and password!");
    }
}

/**
 * Set setting in the IrGGu-server.
 *
 * @param *data Setting and value.
 */
void setSetting (const char *data)
{
    QString command = IrGGu_IRC_Write::newSettingValue;
    QString setting;
    QString name;
    QString value;

    if ( utf8 )
    {
        setting = QString::fromUtf8(data);
    }
    else
    {
        setting = codec->toUnicode(data);
    }

    if ( setting.contains(' ') )
    {
        name  = setting.section(' ', 0, 0);
        value = setting.section(' ', 1);

        QRegularExpression colorRe("^(((local_|mirc_)([0-9]|1[0-5]))|foreground)$");
        QRegularExpressionMatch colorRem = colorRe.match(name);

        if ( name == "scrollback" )
        {
            saveInt("scrollback", value.toInt());

            if ( isConnected )
            {
                command.replace("<category>", name);
                command.replace("<setting>", name);
                command.replace("<value>", value);

                connection->write(command);
            }
            else
            {
                writeClientNotice("Not connected to server!");
            }
        }
        else if ( colorRem.hasMatch() )
        {
            QRegularExpression valueRe("^#[0-9a-fxA-FX]{6}$");
            QRegularExpressionMatch valueRem = valueRe.match(value);

            if ( valueRem.hasMatch() )
            {
                saveStr(name.toUtf8().data(), value.toUtf8().data());

                if ( isConnected )
                {
                    command.replace("<category>", "colors");
                    command.replace("<setting>", name);
                    command.replace("<value>", value);

                    connection->write(command);
                }
            }
            else
            {
                writeClientNotice("Color must be html hex color!");
            }
        }
        else if ( name == "timestampState" )
        {
            saveStr("timestampState", value.toUtf8().data());

            if ( isConnected )
            {
                command.replace("<category>", "timestamp");
                command.replace("<setting>", "state");
                command.replace("<value>", value);

                connection->write(command);
            }
            else
            {
                writeClientNotice("Not connected to server!");
            }
        }
        else if ( name == "timestampFormat" )
        {
            saveStr("timestampFormat", value.toUtf8().data());

            if ( isConnected )
            {
                command.replace("<category>", "timestamp");
                command.replace("<setting>", "format");
                command.replace("<value>", value);

                connection->write(command);
            }
            else
            {
                writeClientNotice("Not connected to server!");
            }
        }
        else
        {
            writeClientNotice("No such setting!");
        }
    }
    else
    {
        writeClientNotice("Not enough parameters! Give name of the setting and value!");
    }
}

/**
 * Insert status window.
 *
 * @param *win Status window.
 */
void insertStatus (WINDOW_REC *win)
{
    statusWindow = win;
}

/**
 * Send insert network command to IrGGu-server.
 *
 * @param *name   Server name.
 * @param *server Server.
 */
void insertServer (char *name, SERVER_REC *server)
{
    QString serverName;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(name);
    }
    else
    {
        serverName = codec->toUnicode(name);
    }

    serversList.insert(serverName, server);

    QString command = IrGGu_IRC_Write::insertNetwork;

    command.replace("<network>", serverName);

    connection->write(command);
}

/**
 * Send insert chat command to IrGGu-server.
 *
 * @param *name   Chat name.
 * @param *server Server name.
 * @param *item   Window item.
 * @param *nicks  Nicks.
 */
void insertChat (char *name, char *server, WI_ITEM_REC *item, GSList *nicks)
{
    QString serverName;
    QString chatName;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(server);
        chatName   = QString::fromUtf8(name);
    }
    else
    {
        serverName = codec->toUnicode(server);
        chatName   = codec->toUnicode(name);
    }

    QMap<QString, WI_ITEM_REC *> tmp = chatsList.value(serverName);
    tmp.insert(chatName, item);
    chatsList.insert(serverName, tmp);

    QString command = IrGGu_IRC_Write::insertChat;

    command.replace("<network>", serverName);
    command.replace("<chat>", chatName);

    connection->write(command);

    if ( nicks != NULL )
    {
        QString names;
        QString prefix;
        GSList *tmp2;
        NICK_REC *nick;

        for ( tmp2 = nicks; tmp2 != NULL; tmp2 = tmp2->next )
        {
            nick   = (NICK_REC *)tmp2->data;
            prefix = nick->op ? "@" : nick->halfop ? "%" : nick->voice ? "+" : "";
            names += prefix + QString::fromUtf8(nick->nick) + " ";
        }

        command = IrGGu_IRC_Write::insertNicks;

        command.replace("<network>", serverName);
        command.replace("<chat>", chatName);
        command.replace("<nicks>", names);

        connection->write(command);
    }
}

/**
 * Send insert nick command to IrGGu-server.
 *
 * @param *server Server name.
 * @param *chat   Chat name.
 * @param *nick   Nick.
 * @param *prefix Nick prefix.
 */
void insertNick (char *server, char *chat, char *nick, char *prefix)
{
    QString serverName;
    QString chatName;
    QString nickName;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(server);
        chatName   = QString::fromUtf8(chat);
        nickName   = QString::fromUtf8(prefix) + QString::fromUtf8(nick);
    }
    else
    {
        serverName = codec->toUnicode(server);
        chatName   = codec->toUnicode(chat);
        nickName   = codec->toUnicode(prefix) + codec->toUnicode(nick);
    }

    QString command = IrGGu_IRC_Write::insertNick;

    command.replace("<network>", serverName);
    command.replace("<chat>", chatName);
    command.replace("<nick>", nickName);

    connection->write(command);
}

/**
 * Send remove network command to IrGGu-server.
 *
 * @param *name Server name.
 */
void removeServer (char *name)
{
    QString serverName;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(name);
    }
    else
    {
        serverName = codec->toUnicode(name);
    }

    serversList.remove(serverName);
    chatsList.remove(serverName);

    QString command = IrGGu_IRC_Write::removeNetwork;

    command.replace("<network>", serverName);

    connection->write(command);
}

/**
 * Send remove chat command to IrGGu-server.
 *
 * @param *name   Chat name.
 * @param *server Server name.
 */
void removeChat (char *name, char *server)
{
    QString serverName;
    QString chatName;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(server);
        chatName   = QString::fromUtf8(name);
    }
    else
    {
        serverName = codec->toUnicode(server);
        chatName   = codec->toUnicode(name);
    }

    QMap<QString, WI_ITEM_REC *> tmp = chatsList.value(serverName);

    if ( !tmp.isEmpty() )
    {
        tmp.remove(chatName);
        chatsList.insert(serverName, tmp);

        QString command = IrGGu_IRC_Write::removeChat;

        command.replace("<network>", serverName);
        command.replace("<chat>", chatName);

        connection->write(command);
    }
}

/**
 * Send remove nick command to IrGGu-server.
 *
 * @param *server Server name.
 * @param *chat   Chat name.
 * @param *nick   Nick.
 * @param *prefix Nick prefix.
 */
void removeNick (char *server, char *chat, char *nick, char *prefix)
{
    QString serverName;
    QString chatName;
    QString nickName;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(server);
        chatName   = QString::fromUtf8(chat);
        nickName   = QString::fromUtf8(prefix) + QString::fromUtf8(nick);
    }
    else
    {
        serverName = codec->toUnicode(server);
        chatName   = codec->toUnicode(chat);
        nickName   = codec->toUnicode(prefix) + codec->toUnicode(nick);
    }

    QString command = IrGGu_IRC_Write::removeNick;

    command.replace("<network>", serverName);
    command.replace("<chat>", chatName);
    command.replace("<nick>", nickName);

    connection->write(command);
}

/**
 * Send change nick command to IrGGu-server.
 *
 * @param *server  Server name.
 * @param *chat    Chat name.
 * @param *nick    Nick.
 * @param *oldNick Old nick.
 * @param *prefix  Nick prefix.
 */
void changeNick (char *server, char *chat, char *nick, char *oldNick, char *prefix)
{
    QString serverName;
    QString chatName;
    QString newNickName;
    QString oldNickName;

    if ( utf8 )
    {
        serverName  = QString::fromUtf8(server);
        chatName    = QString::fromUtf8(chat);
        newNickName = QString::fromUtf8(prefix) + QString::fromUtf8(nick);
        oldNickName = QString::fromUtf8(prefix) + QString::fromUtf8(oldNick);
    }
    else
    {
        serverName  = codec->toUnicode(server);
        chatName    = codec->toUnicode(chat);
        newNickName = codec->toUnicode(prefix) + codec->toUnicode(nick);
        oldNickName = codec->toUnicode(prefix) + codec->toUnicode(oldNick);
    }

    QString command = IrGGu_IRC_Write::changeNick;

    command.replace("<network>", serverName);
    command.replace("<chat>", chatName);
    command.replace("<oldNick>", oldNickName);
    command.replace("<newNick>", newNickName);

    connection->write(command);
}

/**
 * Send change nick mode command to IrGGu-server.
 *
 * @param *server Server name.
 * @param *chat   Chat name.
 * @param *nick   Nick.
 * @param *prefix Nick prefix.
 */
void changeNickMode (char *server, char *chat, char *nick, char *prefix)
{
    QString serverName;
    QString chatName;
    QString nickName;
    QString nickPrefix;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(server);
        chatName   = QString::fromUtf8(chat);
        nickName   = QString::fromUtf8(nick);
        nickPrefix = QString::fromUtf8(prefix);
    }
    else
    {
        serverName = codec->toUnicode(server);
        chatName   = codec->toUnicode(chat);
        nickName   = codec->toUnicode(nick);
        nickPrefix = codec->toUnicode(prefix);
    }

    QString command = IrGGu_IRC_Write::changeNickMode;

    command.replace("<network>", serverName);
    command.replace("<chat>", chatName);
    command.replace("<nick>", nickName);
    command.replace("<mode>", nickPrefix);

    connection->write(command);
}

/**
 * Send rename chat command to IrGGu-server.
 *
 * @param *server      Server name.
 * @param *oldChatName Old chat name.
 * @param *newChatName New chat name.
 */
void renameChat (char *server, char *oldChatName, char *newChatName)
{
    QString serverName;
    QString oldChat;
    QString newChat;

    if ( utf8 )
    {
        serverName = QString::fromUtf8(server);
        oldChat    = QString::fromUtf8(oldChatName);
        newChat    = QString::fromUtf8(newChatName);
    }
    else
    {
        serverName = codec->toUnicode(server);
        oldChat    = codec->toUnicode(oldChatName);
        newChat    = codec->toUnicode(newChatName);
    }

    QMap<QString, WI_ITEM_REC *> chats = chatsList.value(serverName);
    WI_ITEM_REC *                chat  = chats.value(oldChatName, NULL);

    if ( chat )
    {
        chats.remove(oldChatName);
        chats.insert(newChatName, chat);
        chatsList.insert(serverName, chats);

        QString command = IrGGu_IRC_Write::renameChat;

        command.replace("<network>", serverName);
        command.replace("<oldChat>", oldChat);
        command.replace("<newChat>", newChat);

        connection->write(command);
    }
}

/**
 * Send new message command to IrGGu-server.
 *
 * @param *nick Nick.
 * @param *msg  Message.
 * @param *dest Destination.
 */
void newMsg (char *nick, char *msg, TEXT_DEST_REC *dest)
{
    WINDOW_REC *window = dest->window;
    QString serverName;
    QString targetName;
    QString nickName = QString::fromUtf8(nick);
    QString line     = nickName + QString::fromUtf8(msg);

    if ( !serversList.isEmpty() )
    {
        bool found = false;
        QMapIterator<QString, QMap<QString, WI_ITEM_REC *> > chatsListIt(chatsList);
        GSList *tmp;

        while ( chatsListIt.hasNext() && !found )
        {
            chatsListIt.next();
            QMapIterator<QString, WI_ITEM_REC *> chatsIt(chatsListIt.value());

            while ( chatsIt.hasNext() && !found )
            {
                chatsIt.next();
                tmp = window->items;

                while ( tmp != NULL && !found )
                {
                    if ( tmp->data == chatsIt.value() )
                    {
                        found      = true;
                        targetName = chatsIt.key();
                        serverName = chatsListIt.key();
                    }

                    tmp = tmp->next;
                }
            }
        }

        if ( !found )
        {
            QMapIterator<QString, SERVER_REC *> serversListIt(serversList);

            while ( serversListIt.hasNext() && !found )
            {
                serversListIt.next();

                if ( dest->server == serversListIt.value() )
                {
                    found      = true;
                    serverName = serversListIt.key();

                    if ( window == statusWindow )
                    {
                        targetName = "(server)";
                    }
                    else
                    {
                        targetName = "(current)";
                    }
                }
            }

            if ( !found )
            {
                serverName = "(current)";
                targetName = "(current)";
            }
        }
    }
    else
    {
        serverName = "(none)";
        targetName = "(none)";
    }

    QString command = IrGGu_IRC_Write::newMsg;

    command.replace("<network>", serverName);
    command.replace("<chat>", targetName);
    command.replace("<msg>", line);

    connection->write(command);
}

/**
 * Send new highlight message command to IrGGu-server.
 *
 * @param *nick Nick.
 * @param *msg  Message.
 * @param *dest Destination.
 */
void newHightlightMsg (char *nick, char *msg, TEXT_DEST_REC *dest)
{
    WINDOW_REC *window = dest->window;
    QString serverName;
    QString targetName;
    QString nickName = QString::fromUtf8(nick);
    QString line     = nickName + QString::fromUtf8(msg);

    if ( !chatsList.isEmpty() )
    {
        bool found = false;
        QMapIterator<QString, QMap<QString, WI_ITEM_REC *> > chatsListIt(chatsList);
        GSList *tmp;

        while ( chatsListIt.hasNext() && !found )
        {
            chatsListIt.next();
            QMapIterator<QString, WI_ITEM_REC *> chatsIt(chatsListIt.value());

            while ( chatsIt.hasNext() && !found )
            {
                chatsIt.next();
                tmp = window->items;

                while ( tmp != NULL && !found )
                {
                    if ( tmp->data == chatsIt.value() )
                    {
                        found      = true;
                        targetName = chatsIt.key();
                        serverName = chatsListIt.key();

                        QString command = IrGGu_IRC_Write::newHighlightMsg;

                        command.replace("<network>", serverName);
                        command.replace("<chat>", targetName);
                        command.replace("<msg>", line);

                        connection->write(command);
                    }

                    tmp = tmp->next;
                }
            }
        }
    }
}

/**
 * Send new own message command to IrGGu-server.
 *
 * @param *nick Nick.
 * @param *msg  Message.
 * @param *dest Destination.
 */
void newOwnMsg (char *nick, char *msg, TEXT_DEST_REC *dest)
{
    WINDOW_REC *window = dest->window;
    QString serverName;
    QString targetName;
    QString nickName = QString::fromUtf8(nick);
    QString line     = nickName + QString::fromUtf8(msg);

    if ( !chatsList.isEmpty() )
    {
        bool found = false;
        QMapIterator<QString, QMap<QString, WI_ITEM_REC *> > chatsListIt(chatsList);
        GSList *tmp;

        while ( chatsListIt.hasNext() && !found )
        {
            chatsListIt.next();
            QMapIterator<QString, WI_ITEM_REC *> chatsIt(chatsListIt.value());

            while ( chatsIt.hasNext() && !found )
            {
                chatsIt.next();
                tmp = window->items;

                while ( tmp != NULL && !found )
                {

                    if ( tmp->data == chatsIt.value() )
                    {
                        found      = true;
                        targetName = chatsIt.key();
                        serverName = chatsListIt.key();

                        QString command = IrGGu_IRC_Write::newOwnMsg;

                        command.replace("<network>", serverName);
                        command.replace("<chat>", targetName);
                        command.replace("<msg>", line);

                        connection->write(command);
                    }

                    tmp = tmp->next;
                }
            }
        }
    }
}

/**
 * Send new dcc file command to IrGGu-server.
 *
 * @param *nick   Sender.
 * @param *file   File.
 * @param *server Server name.
 */
void newDccFile (char *nick, char *file, char *server)
{
    QString sender;
    QString fileName;
    QString serverName;

    if ( utf8 )
    {
        sender     = QString::fromUtf8(nick);
        fileName   = QString::fromUtf8(file);
        serverName = QString::fromUtf8(server);
    }
    else
    {
        sender     = codec->toUnicode(nick);
        fileName   = codec->toUnicode(file);
        serverName = codec->toUnicode(server);
    }

    QString command = IrGGu_IRC_Write::newDccFile;

    command.replace("<network>", serverName);
    command.replace("<sender>", sender);
    command.replace("<file>", fileName);

    connection->write(command);
}

/**
 * Send close dcc file command to IrGGu-server.
 *
 * @param *nick   Sender.
 * @param *file   File.
 * @param *server Server name.
 */
void closeDccFile (char *nick, char *file, char *server)
{
    QString sender;
    QString fileName;
    QString serverName;

    if ( utf8 )
    {
        sender     = QString::fromUtf8(nick);
        fileName   = QString::fromUtf8(file);
        serverName = QString::fromUtf8(server);
    }
    else
    {
        sender     = codec->toUnicode(nick);
        fileName   = codec->toUnicode(file);
        serverName = codec->toUnicode(server);
    }

    QString command = IrGGu_IRC_Write::closeDccFile;

    command.replace("<network>", serverName);
    command.replace("<sender>", sender);
    command.replace("<file>", fileName);

    connection->write(command);
}
