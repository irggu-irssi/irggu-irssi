/****************************************************************************
**  irggu_misc.h
**
**  Copyright information
**
**      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRGGU_MISC_H
#define IRGGU_MISC_H

#define UOFF_T_LONG_LONG 1

#include "common.h"
#include "formats.h"
#include <QString>
#include <QByteArray>

void silentMessageOutput (QtMsgType type, const QMessageLogContext &context, const QString &msg);

extern "C" void disableMessageOutput();
extern "C" char *appendText(char *newText, char *oldText, int bgcolor, int fgcolor, int flags);
extern "C" char *formatNick(char *nick);
extern "C" char *concatStrings(char *string1, char *string2);
extern "C" char *intToString(int integer);
extern "C" void debug(char *debug);
extern "C" void debugInt(int *debug);

#endif // IRGGU_MISC_H
