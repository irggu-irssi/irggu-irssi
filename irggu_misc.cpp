/****************************************************************************
**  irggu_misc.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "irggu_misc.h"
#include <QTextCodec>
#include <QDebug>

extern "C" int utf8;
extern QTextCodec *codec;

/**
 * Silent message output.
 *
 * @param type    Message type.
 * @param context Message context.
 * @param msg     Message.
 */
void silentMessageOutput (QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
}

/**
 * Disable Message output.
 */
void disableMessageOutput ()
{
    qInstallMessageHandler(silentMessageOutput);
}

/**
 * Append text.
 *
 * @param *newText New text that is going to be append.
 * @param *oldText Old text in which new text is going to be appended.
 * @param bgcolor  Background color.
 * @param fgcolor  Foreground color.
 * @param flags    Flags.
 * @return Complete text.
 */
char *appendText (char *newText, char *oldText, int bgcolor, int fgcolor, int flags)
{
    QString nText;
    QString oText;

    if ( utf8 )
    {
        nText = QString::fromUtf8(newText);
    }
    else
    {
        nText = codec->toUnicode(newText);
    }

    if ( oldText )
    {
        oText = QString::fromUtf8(oldText);
    }
    else
    {
        oText = "";
    }


    nText.replace('<', "&lt;");
    nText.replace('>', "&gt;");
    nText.replace(' ', "&nbsp;");

    QString bg        = "";
    QString fg        = "";
    QString bold      = "";
    QString underline = "";

    if ( flags & GUI_PRINT_FLAG_MIRC_COLOR )
    {
        if ( bgcolor >= 0 && bgcolor <= 15 )
        {
            bg = "background-color: <mirc_" + QString::number(bgcolor) + ">;";
        }

        if ( fgcolor >= 0 && fgcolor <= 15 )
        {
            fg = "color: <mirc_" + QString::number(fgcolor) + ">;";
        }
        else
        {
            fg = "color: <foreground>;";
        }
    }
    else
    {
        if ( fgcolor >= 0 && fgcolor <= 15 )
        {
            fg = "color: <local_" + QString::number(fgcolor) + ">;";
        }
        else
        {
            fg = "color: <foreground>;";
        }
    }

    if ( flags & GUI_PRINT_FLAG_BOLD )
    {
        bold = "font-weight: bold;";
    }

    if ( flags & GUI_PRINT_FLAG_UNDERLINE )
    {
        underline = "text-decoration: underline;";
    }

    nText = "<span style='" + bg + fg + bold + underline + "'>"
            + nText + "</span>";

    QString    tmp  = oText + nText;
    QByteArray text = tmp.toUtf8();

    if ( oldText )
    {
        delete [] oldText;
    }

    oldText = new char[text.size() + 1];
    strcpy(oldText, text.data());

    return oldText;
}

/**
 * Format nick.
 *
 * @param *nick Nick to be formatted.
 * @return Formatted nick.
 */
char *formatNick (char *nick)
{
    QString tmp = "<nick>" + QString::fromUtf8(nick) + "</nick>";

    int i = tmp.lastIndexOf("&nbsp;");

    if ( i == tmp.size() - 20 )
    {
        tmp.remove(i, 6);
    }

    QByteArray nickName = tmp.toUtf8();

    char *formattedNick = new char[nickName.size() + 1];

    delete [] nick;

    strcpy(formattedNick, nickName.data());

    return formattedNick;
}

/**
 * Concats to strings.
 *
 * @param string1 First string to concat
 * @param string2 Second string to concat
 * @return Concatenated string.
 */
char *concatStrings (char *string1, char *string2)
{
    char *string;

    if ( utf8 )
    {
        QString concat = QString::fromUtf8(string1);

        concat.append(QString::fromUtf8(string2));

        QByteArray text = concat.toUtf8();

        string = new char[text.size() + 1];
        strcpy(string, text.data());
    }
    else
    {
        QString concat = codec->toUnicode(string1);

        concat.append(codec->toUnicode(string2));

        QByteArray text = concat.toUtf8();

        string = new char[text.size() + 1];
        strcpy(string, text.data());
    }

    return string;
}

/**
 * Convert integer to string.
 *
 * @param integer Integer that is going to be converted to string.
 * @return Intefer as string.
 */
char *intToString (int integer)
{
    char *string;

    if ( utf8 )
    {
        QByteArray text = QString::number(integer).toUtf8();

        string = new char[text.size() + 1];
        strcpy(string, text.data());
    }
    else
    {
        QByteArray text = codec->fromUnicode(QString::number(integer).toUtf8());

        string = new char[text.size() + 1];
        strcpy(string, text.data());
    }

    return string;
}

/**
 * Debug string.
 *
 * @param *debug String to debug.
 */
void debug (char *debug)
{
    qDebug() << debug << endl;
}

/**
 * Debug integer.
 *
 * @param *debug Integer to debug.
 */
void debugInt (int *debug)
{
    qDebug() << debug << endl;
}
