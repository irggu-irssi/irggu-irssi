/****************************************************************************
**  handler_irssi.h
**
**  Copyright information
**
**      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef HANDLER_H
#define HANDLER_H

#include "irggu_irssi.h"

#define IS_COLOR_CODE(c) \
        ((c) == 2 || (c) == 3 || (c) == 4 || (c) == 6 || (c) == 7 || \
        (c) == 15 || (c) == 22 || (c) == 27 || (c) == 31)

void handlerIrssiInit();
void handlerIrssiDeinit();

extern void       changeServerIrssi(WINDOW_REC *win, SERVER_REC *server);
extern void       writeIrssi(char *line, SERVER_REC *server, WI_ITEM_REC *item);
extern void       writeClientNotice(const char *line);
extern void       registered();
extern void       unregistered();
extern void       saveStr(char *key, char *str);
extern void       saveInt(char *key, int value);
extern void       saveBool(char *key, int value);
extern void       setWindowActive(WINDOW_REC *window);
extern const char *getStr(char *key);
extern const int  getInt(char *key);
extern const int  getBool(char *key);
extern char       *completeIrssi(char *line, int pos, WINDOW_REC *win);
extern WINDOW_REC *findWindow(SERVER_REC *server, const char *name);

#endif // HANDLER_H
