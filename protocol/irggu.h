/****************************************************************************
**  irggu.h
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRGGU_H
#define IRGGU_H

#include <QString>

namespace IrGGu_IRC
{
    const float version = 1.0;
}

namespace IrGGu_IRC_Handshake
{
    const QString receive = "^IRGGU_IRC_PROTOCOL_VERSION\\s(?<version>[1-9][0-9]*\\.[0-9])$";
    const QString write   = "IRGGU_IRC_PROTOCOL_VERSION <version>";
}

namespace IrGGu_IRC_Receive
{
    const QString clientNotice       = "^CLIENT_NOTICE\\s(?<lines>(?:<line>.+</line>)+)$";
    const QString registerIRC        = "^REGISTER\\s(?<result>[0-9])$";
    const QString writeLine          = "^WRITE_LINE\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<line>.+)$";
    const QString completeLine       = "^COMPLETE_LINE\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<pos>[1-9][0-9]*)\\s(?<line>.+)$";
    const QString acceptDccFile      = "^ACCEPT_DCC_FILE\\s(?<network>\\S+)\\s(?<sender>\\S+)\\s(?<file>.+)$";
    const QString doNotAcceptDccFile = "^DO_NOT_ACCEPT_DCC_FILE\\s(?<network>\\S+)\\s(?<sender>\\S+)\\s(?<file>.+)$";
    const QString quitNetwork        = "^QUIT\\s(?<network>\\S+)$";
    const QString closeChat          = "^CLOSE_CHAT\\s(?<network>\\S+)\\s(?<chat>\\S+)$";
    const QString query              = "^QUERY\\s(?<network>\\S+)\\s(?<nick>\\S+)$";
}

namespace IrGGu_IRC_Write
{
    const QString registerIRC      = "REGISTER <username> <password>";
    const QString registerForceIRC = "REGISTER_FORCE <username> <password>";
    const QString newSettingValue  = "SETTING <category> <setting> <value>";
    const QString setColors        = "SET_COLORS <colors>";
    const QString insertNetwork    = "INSERT_NETWORK <network>";
    const QString insertChat       = "INSERT_CHAT <network> <chat>";
    const QString insertNick       = "INSERT_NICK <network> <chat> <nick>";
    const QString insertNicks      = "INSERT_NICKS <network> <chat> <nicks>";
    const QString removeNetwork    = "REMOVE_NETWORK <network>";
    const QString removeChat       = "REMOVE_CHAT <network> <chat>";
    const QString removeNick       = "REMOVE_NICK <network> <chat> <nick>";
    const QString changeNick       = "CHANGE_NICK <network> <chat> <oldNick> <newNick>";
    const QString changeNickMode   = "CHANGE_NICK_MODE <network> <chat> <nick> <mode>";
    const QString renameChat       = "RENAME_CHAT <network> <oldChat> <newChat>";
    const QString newMsg           = "NEW_MSG <network> <chat> <msg>";
    const QString newHighlightMsg  = "NEW_HIGHLIGHT_MSG <network> <chat> <msg>";
    const QString newOwnMsg        = "NEW_OWN_MSG <network> <chat> <msg>";
    const QString newDccFile       = "NEW_DCC_FILE <network> <sender> <file>";
    const QString closeDccFile     = "CLOSE_DCC_FILE <network> <sender> <file>";
    const QString lineCompleted    = "LINE_COMPLETED <network> <chat> <line>";
}

#endif // IRGGU_H
