/****************************************************************************
**  connection.h
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CONNECTION_H
#define CONNECTION_H

#include "network/localsocket.h"
#include <QObject>
#include <QString>
#include <QMap>

typedef void stateFunction();
typedef void msgFunction(QString);
typedef void handleFunction(QString);
typedef QMap<QString, handleFunction *> handleFunctionList;

/**
 *  This class is for handling connection to IrGGu-server.
 *
 *  @author     Arttu Liimola <arttu.liimola@gmail.com>
 *  @version    2013-09-17
 */
class Connection : public QObject
{
    Q_OBJECT
public:
    explicit Connection(QObject *parent = 0);

    void setFunctions(stateFunction *connected, stateFunction *disconnected,
                      msgFunction *printMsg, handleFunctionList handleFunctions);
    void connectToServer();
    void disconnect();
    void write(QString line);

private:
    LocalSocket        *socket;
    stateFunction      *connectedFunction;
    stateFunction      *disconnectedFunction;
    msgFunction        *printMsgFunction;
    handleFunctionList handleFunctions;

private Q_SLOTS:
    void connected();
    void disconnected();
    void error(QLocalSocket::LocalSocketError error);
    void reconnect();
    void readData();

};

#endif // CONNECTION_H
