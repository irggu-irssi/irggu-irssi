/****************************************************************************
**  connection.cpp
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "connection.h"
#include <QTimer>

/**
 * Constructor.
 */
Connection::Connection (QObject *parent) : QObject(parent)
{
    socket               = new LocalSocket(this);
    connectedFunction    = NULL;
    disconnectedFunction = NULL;

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket, SIGNAL(error(QLocalSocket::LocalSocketError)), this,
            SLOT(error(QLocalSocket::LocalSocketError)));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
}

void Connection::setFunctions (stateFunction *connected, stateFunction *disconnected,
                               msgFunction *printMsg, handleFunctionList handleFunctions)
{
    this->connectedFunction    = connected;
    this->disconnectedFunction = disconnected;
    this->printMsgFunction     = printMsg;
    this->handleFunctions      = handleFunctions;
}

/**
 * Connects to server.
 */
void Connection::connectToServer ()
{
    QString msg = "Connecting to IrGGu server.";

    printMsgFunction(msg);

    socket->connectToServer(SOCK_FILE);
}

/**
 * Send line to server.
 *
 * @param line Line to write.
 */
void Connection::write (QString line)
{
    line += "\n";

    socket->write(line.toUtf8());
}

/**
 * Disconnect from server.
 */
void Connection::disconnect ()
{
    socket->disconnectFromServer();
}

void Connection::connected ()
{
    connectedFunction();
}

void Connection::disconnected ()
{
    disconnectedFunction();
}

void Connection::error (QLocalSocket::LocalSocketError error)
{
    switch ( error )
    {
        case QLocalSocket::ConnectionRefusedError:
        case QLocalSocket::SocketAccessError:
        case QLocalSocket::SocketResourceError:
        {
            QString msg = "Unable to connect to IrGGu server!";

            printMsgFunction(msg);

            msg = "Reconnecting in 60 seconds.";

            printMsgFunction(msg);

            QTimer::singleShot(60000, this, SLOT(reconnect()));

            break;
        }

        case QLocalSocket::PeerClosedError:
        {
            QString msg = "Server closed the connection!";

            printMsgFunction(msg);

            msg = "Reconnecting in 60 seconds.";

            printMsgFunction(msg);

            QTimer::singleShot(60000, this, SLOT(reconnect()));

            break;
        }

        case QLocalSocket::ServerNotFoundError:
        {
            QString msg = "Unable to find IrGGu server!";

            printMsgFunction(msg);

            msg = "Reconnecting in 60 seconds.";

            printMsgFunction(msg);

            QTimer::singleShot(60000, this, SLOT(reconnect()));

            break;
        }
    }
}

void Connection::reconnect ()
{
    connectToServer();
}

void Connection::readData ()
{
    QString line;
    QString command;

    while ( socket->canReadLine() )
    {
        line    = QString::fromUtf8(socket->readLine().trimmed());
        command = line.section(" ", 0, 0);

        handleFunction *function = handleFunctions.value(command, NULL);

        if ( function )
        {
            function(line);
        }
    }
}
