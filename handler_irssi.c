/****************************************************************************
**  handler_irssi.c
**
**  Copyright information
**
**      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "handler_irssi.h"

static GList *complist;
static char  *last_line;
static int   last_want_space;
static int   last_line_pos;
static int   isRegistered = 0;
static int   ownMsg       = 0;
int          utf8         = 0;

extern void init();
extern void deinit();
extern void connectToServer();
extern void disconnect();
extern void registerToServer(const char *data, int force);
extern void setSetting(const char *data);
extern void insertStatus(WINDOW_REC *win);
extern void insertServer(char *name, SERVER_REC *server);
extern void insertChat(char *name, char *server, WI_ITEM_REC *item, GSList *nicks);
extern void insertNick(char *server, char *channel, char *nick, char *prefix);
extern void removeServer(char *name);
extern void removeChat(char *name, char *server);
extern void removeNick(char *server, char *channel, char *nick, char *prefix);
extern void changeNick(char *server, char *channel, char *nick, char *oldNick, char *prefix);
extern void changeNickMode(char *server, char *channel, char *nick, char *prefix);
extern void renameChat(char *server, char *oldChatName, char *newChatName);
extern void newMsg(char *nick, char *msg, WINDOW_REC *window);
extern void newHightlightMsg(char *nick, char *msg, WINDOW_REC *window);
extern void newOwnMsg(char *nick, char *msg, WINDOW_REC *window);
extern void newDccFile(char *nick, char *file, char *server);
extern void closeDccFile(char *nick, char *file, char *server);
extern void disableMessageOutput();
extern char *appendText(char *newText, char *oldText, int bgcolor, int fgcolor, int flags);
extern char *formatNick(char *nick);
extern char *concatStrings(char *string1, char *string2);
extern char *intToString(int integer);
extern void debug(char *debug);
extern void debugInt(int *debug);

static void       cmdIrGGu(const char *data, SERVER_REC *server, WI_ITEM_REC *item);
static void       cmdHelp();
static void       cmdRegister(const char *data);
static void       cmdRegisterForce(const char *data);
static void       cmdSetting(const char *data, SERVER_REC *server, WI_ITEM_REC *item);
static void       cmdSettingSet(const char *data);
static void       cmdSettingList();
static void       cmdColors(const char *data, SERVER_REC *server, WI_ITEM_REC *item);
static void       cmdColorsSet(const char *data);
static void       cmdColorsList();
static void       cmdAutoregister(const char *data);
static void       connectSignals();
static void       disconnectSignals();
static void       getServersAndChats();
static void       serverLooking(SERVER_REC *server);
static void       serverDisconnected(SERVER_REC *server);
static void       channelCreated(CHANNEL_REC *channel);
static void       channelDestroyed(CHANNEL_REC *channel);
static void       queryCreated(QUERY_REC *query);
static void       queryDestroyed(QUERY_REC *query);
static void       queryRenamed(QUERY_REC *query, char *oldChatName);
static void       msg(TEXT_DEST_REC *dest, const char *text);
static void       msgOwn();
static void       dcc(GET_DCC_REC *dcc);
static void       closeDcc(GET_DCC_REC *dcc);
static void       nickAdded(CHANNEL_REC *channel, NICK_REC *nick);
static void       nickRemoved(CHANNEL_REC *channel, NICK_REC *nick);
static void             nickChanged(CHANNEL_REC *channel, NICK_REC *nick, char *oldNick);
static void       nickModeChanged(CHANNEL_REC *channel, NICK_REC *nick);
static void       get_mirc_color(const char **str, int *fg_ret, int *bg_ret);
static const char *get_ansi_color(THEME_REC *theme, const char *str, int *fg_ret, int *bg_ret,
                                  int *flags_ret);
static char       *get_word_at(const char *str, int pos, char **startpos);
static void       free_completions();


#define isseparator_notspace(c) \
        ((c) == ',')

#define isseparator(c) \
    ((c) == ' ' || isseparator_notspace(c))

/**
 * Handles module init.
 */
void handlerIrssiInit ()
{
    disableMessageOutput();

    command_bind("irggu", NULL, (SIGNAL_FUNC) cmdIrGGu);
    command_bind("irggu help", NULL, (SIGNAL_FUNC) cmdHelp);
    command_bind("irggu register", NULL, (SIGNAL_FUNC) cmdRegister);
    command_bind("irggu register-force", NULL, (SIGNAL_FUNC) cmdRegisterForce);
    command_bind("irggu setting", NULL, (SIGNAL_FUNC) cmdSetting);
    command_bind("irggu setting set", NULL, (SIGNAL_FUNC) cmdSettingSet);
    command_bind("irggu setting list", NULL, (SIGNAL_FUNC) cmdSettingList);
    command_bind("irggu colors", NULL, (SIGNAL_FUNC) cmdColors);
    command_bind("irggu colors set", NULL, (SIGNAL_FUNC) cmdColorsSet);
    command_bind("irggu colors list", NULL, (SIGNAL_FUNC) cmdColorsList);
    command_bind("irggu autoregister", NULL, (SIGNAL_FUNC) cmdAutoregister);

    insertStatus(window_find_name("(status)"));

    settings_add_str_module("irggu", "irggu", "username", "");
    settings_add_str_module("irggu", "irggu", "password", "");
    settings_add_int_module("irggu", "irggu", "scrollback", 500);
    settings_add_str_module("irggu", "irggu", "timestampState", "enabled");
    settings_add_str_module("irggu", "irggu", "timestampFormat", "[dd.MM.yy hh:mm:ss]");
    settings_add_str_module("irggu", "irggu", "foreground", "#000000");
    settings_add_str_module("irggu", "irggu", "local_0", "#CCCCCC");
    settings_add_str_module("irggu", "irggu", "local_1", "#000000");
    settings_add_str_module("irggu", "irggu", "local_2", "#3636B2");
    settings_add_str_module("irggu", "irggu", "local_3", "#2A8C2A");
    settings_add_str_module("irggu", "irggu", "local_4", "#C33B3B");
    settings_add_str_module("irggu", "irggu", "local_5", "#C73232");
    settings_add_str_module("irggu", "irggu", "local_6", "#80267F");
    settings_add_str_module("irggu", "irggu", "local_7", "#66361F");
    settings_add_str_module("irggu", "irggu", "local_8", "#4C4C4C");
    settings_add_str_module("irggu", "irggu", "local_9", "#4545E6");
    settings_add_str_module("irggu", "irggu", "local_10", "#1A5555");
    settings_add_str_module("irggu", "irggu", "local_11", "#2F8C74");
    settings_add_str_module("irggu", "irggu", "local_12", "#4545E6");
    settings_add_str_module("irggu", "irggu", "local_13", "#B037B0");
    settings_add_str_module("irggu", "irggu", "local_14", "#C73232");
    settings_add_str_module("irggu", "irggu", "local_15", "#959595");
    settings_add_str_module("irggu", "irggu", "mirc_0", "#CCCCCC");
    settings_add_str_module("irggu", "irggu", "mirc_1", "#000000");
    settings_add_str_module("irggu", "irggu", "mirc_2", "#3636B2");
    settings_add_str_module("irggu", "irggu", "mirc_3", "#2A8C2A");
    settings_add_str_module("irggu", "irggu", "mirc_4", "#C33B3B");
    settings_add_str_module("irggu", "irggu", "mirc_5", "#C73232");
    settings_add_str_module("irggu", "irggu", "mirc_6", "#80267F");
    settings_add_str_module("irggu", "irggu", "mirc_7", "#66361F");
    settings_add_str_module("irggu", "irggu", "mirc_8", "#D9A641");
    settings_add_str_module("irggu", "irggu", "mirc_9", "#3DCC3D");
    settings_add_str_module("irggu", "irggu", "mirc_10", "#1A5555");
    settings_add_str_module("irggu", "irggu", "mirc_11", "#2F8C74");
    settings_add_str_module("irggu", "irggu", "mirc_12", "#4545E6");
    settings_add_str_module("irggu", "irggu", "mirc_13", "#B037B0");
    settings_add_str_module("irggu", "irggu", "mirc_14", "#4C4C4C");
    settings_add_str_module("irggu", "irggu", "mirc_15", "#959595");
    settings_add_bool_module("irggu", "irggu", "autoregister", FALSE);

    if ( strcmp(getStr("term_charset"), "UTF-8") == 0 )
    {
        utf8 = 1;
    }

    init();
    connectToServer();
}

/**
 * Handles module deinit.
 */
void handlerIrssiDeinit ()
{
    command_unbind("irggu", (SIGNAL_FUNC) cmdIrGGu);
    command_unbind("irggu help", (SIGNAL_FUNC) cmdHelp);
    command_unbind("irggu register", (SIGNAL_FUNC) cmdRegister);
    command_unbind("irggu register-force", (SIGNAL_FUNC) cmdRegisterForce);
    command_unbind("irggu setting", (SIGNAL_FUNC) cmdSetting);
    command_unbind("irggu setting set", (SIGNAL_FUNC) cmdSettingSet);
    command_unbind("irggu setting list", (SIGNAL_FUNC) cmdSettingList);
    command_unbind("irggu colors", (SIGNAL_FUNC) cmdColors);
    command_unbind("irggu colors set", (SIGNAL_FUNC) cmdColorsSet);
    command_unbind("irggu colors list", (SIGNAL_FUNC) cmdColorsList);
    command_unbind("irggu autoregister", (SIGNAL_FUNC) cmdAutoregister);

    disconnectSignals();
    disconnect();
    deinit();
}

/**
 * Change server in Irssi.
 *
 * @param *win    Window.
 * @param *server Server.
 */
void changeServerIrssi (WINDOW_REC *win, SERVER_REC *server)
{
    window_change_server(win, server);
}

/**
 * Write line into IRC.
 *
 * @param *line   Line to write.
 * @param *server Server.
 * @param *item   Window item.
 */
void writeIrssi (char *line, SERVER_REC *server, WI_ITEM_REC *item)
{
    signal_emit("send command", 3, line, server, item);
}

/**
 * Write client notice.
 *
 * @param *line Line to write.
 */
void writeClientNotice (const char *line)
{
    char *irggu = "IrGGu: ";

    printtext(NULL, NULL, MSGLEVEL_CLIENTNOTICE, concatStrings(irggu, line), MODULE);
}

/**
 * Called when client has been registered.
 */
void registered ()
{
    isRegistered = 1;
    connectSignals();
    getServersAndChats();
}

/**
 * Called when client has disconnected.
 */
void unregistered ()
{
    isRegistered = 0;
    disconnectSignals();
}

/**
 * Save string into settings.
 *
 * @param *key Setting key.
 * @param *str Setting value.
 */
void saveStr (char *key, char *str)
{
    settings_set_str(key, str);
}

/**
 * Save integer into settings.
 *
 * @param *key   Setting key.
 * @param *value Setting value.
 */
void saveInt (char *key, int value)
{
    settings_set_int(key, value);
}

/**
 * Save boolean into settings.
 *
 * @param *key   Setting key.
 * @param *value Setting value.
 */
void saveBool (char *key, int value)
{
    settings_set_bool(key, value);
}

/**
 * Set active window in Irssi.
 *
 * @param *window Window.
 */
void setWindowActive (WINDOW_REC *window)
{
    window_set_active(window);
}

/**
 * Get string from settings.
 *
 * @param *key Setting key.
 * @return Setting value.
 */
const char *getStr (char *key)
{
    return settings_get_str(key);
}

/**
 * Get integer from settings.
 *
 * @param *key Setting key.
 * @return Setting value.
 */
const int getInt (char *key)
{
    return settings_get_int(key);
}

/**
 * Get boolean from settings.
 *
 * @param *key Setting key.
 * @return Setting value.
 */
const int getBool (char *key)
{
    return settings_get_bool(key);
}

/**
 * Complete command or nick, this is copy paste from Irssi.
 *
 * @param *line Line to complete.
 * @param pos   Cursor position.
 * @param *win  Window.
 * @return Completed line.
 */
char *completeIrssi (char *line, int pos, WINDOW_REC *win)
{
    static int startpos = 0, wordlen = 0;
    int old_startpos, old_wordlen;

    GString *result;
    char *word, *wordstart, *linestart, *ret;
    int continue_complete, want_space;

    g_return_val_if_fail(line != NULL, NULL);
    g_return_val_if_fail(pos != NULL, NULL);

    continue_complete = complist != NULL && pos == last_line_pos && strcmp(line, last_line) == 0;

    old_startpos = startpos;
    old_wordlen = wordlen;

    if ( continue_complete )
    {
        word      = NULL;
        linestart = NULL;
    }
    else
    {
        /* get the word we want to complete */
        word     = get_word_at(line, pos, &wordstart);
        startpos = (int) (wordstart-line);
        wordlen  = strlen(word);

        /* get the start of line until the word we're completing */
        if ( isseparator(*line) )
        {
            /* empty space at the start of line */
            if ( wordstart == line )
            {
                wordstart += strlen(wordstart);
            }
        }
        else
        {
            while ( wordstart > line && isseparator(wordstart[-1]) )
            {
                wordstart--;
            }
        }
        linestart = g_strndup(line, (int) (wordstart-line));

        /* completions usually add space after the word, that makes
           things a bit harder. When continuing a completion
           "/msg nick1 "<tab> we have to cycle to nick2, etc.
           BUT if we start completion with "/msg "<tab>, we don't
           want to complete the /msg word, but instead complete empty
           word with /msg being in linestart. */
        if ( pos > 0 && line[pos-1] == ' ' && (*linestart == '\0' || wordstart[-1] != ' ') )
        {
            char *old;

            old       = linestart;
            linestart = *linestart == '\0' ? g_strdup(word) : g_strconcat(linestart, " ", word,
                                                                          NULL);
            g_free(old);
            g_free(word);
            word = g_strdup("");
            startpos = strlen(linestart)+1;
            wordlen = 0;
        }

    }

    if ( continue_complete )
    {
        /* complete from old list */
        complist = complist->next != NULL ? complist->next : g_list_first(complist);
        want_space = last_want_space;
    }
    else
    {
        /* get new completion list */
        free_completions();

        want_space = TRUE;
        signal_emit("complete word", 5, &complist, win, word, linestart, &want_space);
        last_want_space = want_space;
    }

    g_free(linestart);
    g_free(word);

    if ( complist == NULL )
    {
        return NULL;
    }

    /* word completed */
    pos = startpos+strlen(complist->data);

    /* replace the word in line - we need to return
       a full new line */
    result = g_string_new(line);
    g_string_erase(result, startpos, wordlen);
    g_string_insert(result, startpos, complist->data);

    if ( want_space )
    {
        if ( !isseparator(result->str[pos]) )
        {
            g_string_insert_c(result, pos, ' ');
        }

        (pos)++;
    }

    wordlen = strlen(complist->data);
    last_line_pos = pos;
    g_free_not_null(last_line);
    last_line = g_strdup(result->str);

    ret = result->str;
    g_string_free(result, FALSE);

    return ret;
}

/**
 * Find window.
 *
 * @param *server Server.
 * @param *name   Window name.
 * @return Window.
 */
WINDOW_REC *findWindow (SERVER_REC *server, const char *name)
{
    return window_find_item(server, name);
}

/**
 * Run subcommand.
 *
 * @param *data   Data that contains subcommand.
 * @param *server Server.
 * @param *item   Window item.
 */
static void cmdIrGGu (const char *data, SERVER_REC *server, WI_ITEM_REC *item)
{
    command_runsub("irggu", data, server, item);
}


/**
 * Run help command.
 */
static void cmdHelp ()
{
    char *s1 = "List of commands: command #explanation";
    char *s2 = "1. irggu register <username> <password>       "
               "#Register to IrGGu-server";
    char *s3 = "2. irggu register-force <username> <password> "
               "#Force registation, removes existing registation";
    char *s4 = "3. irggu setting set <setting> <value>        "
               "#Change setting for your account in IrGGu-server";
    char *s5 = "4. irggu setting list                         "
                "#List all settings";
    char *s6 = "3. irggu colors set <color> <value>           "
               "#Change default color for your account in IrGGu-server";
    char *s7 = "4. irggu colors list                          "
               "#List all colors";
    char *s8 = "5. irggu autoregister enable/disable          "
               "#Enable/Disable automatic registration";


    writeClientNotice(s1);
    writeClientNotice(s2);
    writeClientNotice(s3);
    writeClientNotice(s4);
    writeClientNotice(s5);
    writeClientNotice(s6);
    writeClientNotice(s7);
    writeClientNotice(s8);
}

/**
 * Run register command.
 *
 * @param *data Data that parameters.
 */
static void cmdRegister (const char *data)
{
    registerToServer(data, 0);
}

/**
 * Run register-force command.
 *
 * @param *data Data that parameters.
 */
static void cmdRegisterForce (const char *data)
{
    registerToServer(data, 1);
}

/**
 * Run settings subcommand.
 *
 * @param *data   Data that contains subcommand.
 * @param *server Server.
 * @param *item   Window item.
 */
static void cmdSetting (const char *data, SERVER_REC *server, WI_ITEM_REC *item)
{
    command_runsub("irggu setting", data, server, item);
}

/**
 * Run setting set command.
 *
 * @param *data Data that parameters.
 */
static void cmdSettingSet (const char *data)
{
    setSetting(data);
}

/**
 * Run setting list command.
 */
static void cmdSettingList ()
{
    char *s1 = "List of settings: setting: value";
    char *s2 = concatStrings("1. scrollback:      ", intToString(getInt("scrollback")));
    char *s3 = concatStrings("2. timestampState:  ", getStr("timestampState"));
    char *s4 = concatStrings("2. timestampFormat: ", getStr("timestampFormat"));

    writeClientNotice(s1);
    writeClientNotice(s2);
    writeClientNotice(s3);
    writeClientNotice(s4);
}

/**
 * Run colors subcommand.
 *
 * @param *data   Data that contains subcommand.
 * @param *server Server.
 * @param *item   Window item.
 */
static void cmdColors (const char *data, SERVER_REC *server, WI_ITEM_REC *item)
{
    command_runsub("irggu colors", data, server, item);
}

/**
 * Run colors set command.
 *
 * @param *data Data that parameters.
 */
static void cmdColorsSet (const char *data)
{
    setSetting(data);
}

/**
 * Run colors list command.
 */
static void cmdColorsList ()
{
    char *s1 = "List of colors: color value";

    writeClientNotice(s1);

    char *local = "local_";
    char *mirc  = "mirc_";
    char *n     = ". ";
    char *color;

    int i;
    int j = 1;

    char s2[20];

    sprintf(s2, "%2d. %-11s %7s", j, "foreground", getStr("foreground"));

    writeClientNotice(s2);

    j++;

    for ( i = 0; i < 16; i++ )
    {
        color = concatStrings(local, intToString(i));

        sprintf(s2, "%2d. %-11s %7s", j, color, getStr(color));

        writeClientNotice(s2);

        g_free(color);

        j++;
    }

    for ( i = 0; i < 16; i++ )
    {
        color = concatStrings(mirc, intToString(i));

        sprintf(s2, "%2d. %-11s %7s", j, color, getStr(color));

        writeClientNotice(s2);

        g_free(color);

        j++;
    }
}

static void cmdAutoregister (const char *data)
{
    if ( strcmp(data, "enable") == 0 )
    {
        saveBool("autoregister", 1);

        writeClientNotice("Autoregister is enabled.");
    }
    else if ( strcmp(data, "disable") == 0 )
    {
        saveBool("autoregister", 0);

        writeClientNotice("Autoregister is disabled.");
    }
    else
    {
        writeClientNotice("Parameter must be enable or disable!");
    }
}

/**
 * Connect Irssi signals.
 */
static void connectSignals ()
{
    signal_add_last("server looking", (SIGNAL_FUNC) serverLooking);
    signal_add_last("server disconnected", (SIGNAL_FUNC) serverDisconnected);
    signal_add_last("server connect failed", (SIGNAL_FUNC) serverDisconnected);
    signal_add_last("channel created", (SIGNAL_FUNC) channelCreated);
    signal_add_last("channel destroyed", (SIGNAL_FUNC) channelDestroyed);
    signal_add_last("query created", (SIGNAL_FUNC) queryCreated);
    signal_add_last("query destroyed", (SIGNAL_FUNC) queryDestroyed);
    signal_add_last("query nick changed", (SIGNAL_FUNC) queryRenamed);
    signal_add_last("print text", (SIGNAL_FUNC) msg);
    signal_add_last("message own_public", (SIGNAL_FUNC) msgOwn);
    signal_add_last("message own_private", (SIGNAL_FUNC) msgOwn);
    signal_add_last("dcc request", (SIGNAL_FUNC) dcc);
    signal_add_last("dcc closed", (SIGNAL_FUNC) closeDcc);
    //signal_add_last("dcc connected", (SIGNAL_FUNC) closeDcc);
    signal_add_last("nicklist new", (SIGNAL_FUNC) nickAdded);
    signal_add_last("nicklist remove", (SIGNAL_FUNC) nickRemoved);
    signal_add_last("nicklist changed", (SIGNAL_FUNC) nickChanged);
    signal_add_last("nick mode changed", (SIGNAL_FUNC) nickModeChanged);
}

/**
 * Disconnect Irssi signals.
 */
static void disconnectSignals ()
{
    signal_remove("server looking", (SIGNAL_FUNC) serverLooking);
    signal_remove("server disconnected", (SIGNAL_FUNC) serverDisconnected);
    signal_remove("server connect failed", (SIGNAL_FUNC) serverDisconnected);
    signal_remove("channel created", (SIGNAL_FUNC) channelCreated);
    signal_remove("channel destroyed", (SIGNAL_FUNC) channelDestroyed);
    signal_remove("query created", (SIGNAL_FUNC) queryCreated);
    signal_remove("query destroyed", (SIGNAL_FUNC) queryDestroyed);
    signal_remove("query nick changed", (SIGNAL_FUNC) queryRenamed);
    signal_remove("print text", (SIGNAL_FUNC) msg);
    signal_remove("message own_public", (SIGNAL_FUNC) msgOwn);
    signal_remove("message own_private", (SIGNAL_FUNC) msgOwn);
    signal_remove("dcc request", (SIGNAL_FUNC) dcc);
    signal_remove("dcc closed", (SIGNAL_FUNC) closeDcc);
    //signal_remove("dcc connected", (SIGNAL_FUNC) closeDcc);
    signal_remove("nicklist new", (SIGNAL_FUNC) nickAdded);
    signal_remove("nicklist remove", (SIGNAL_FUNC) nickRemoved);
    signal_remove("nicklist changed", (SIGNAL_FUNC) nickChanged);
    signal_remove("nick mode changed", (SIGNAL_FUNC) nickModeChanged);
}

/**
 * Get server and chats.
 */
static void getServersAndChats ()
{
    GSList *tmp;
    SERVER_REC *server;
    CHANNEL_REC *channel;
    QUERY_REC *query;
    WI_ITEM_REC *item;

    for ( tmp = servers; tmp != NULL; tmp = tmp->next )
    {
        server = tmp->data;
        insertServer(server->tag, server);
    }

    for ( tmp = channels; tmp != NULL; tmp = tmp->next )
    {
        channel = tmp->data;
        item    = window_item_find(channel->server, channel->visible_name);

        if ( item )
        {
            server = channel->server;
            insertChat(channel->visible_name, server->tag, item, nicklist_getnicks(channel));
        }
    }

    for ( tmp = queries; tmp != NULL; tmp = tmp->next )
    {
        query  = tmp->data;
        item = window_item_find(query->server, query->name);

        if ( item )
        {
            server = query->server;
            insertChat(query->name, server->tag, item, NULL);
        }
    }
}

/**
 * Called when emited server looking signal.
 *
 * @param *server Server.
 */
static void serverLooking (SERVER_REC *server)
{
    insertServer(server->tag, server);
}

/**
 * Called when emited server disconnected signal.
 *
 * @param *server Server.
 */
static void serverDisconnected (SERVER_REC *server)
{
    removeServer(server->tag);
}

/**
 * Called when emited channel created signal.
 *
 * @param *channel Channel.
 */
static void channelCreated (CHANNEL_REC *channel)
{
    WI_ITEM_REC *item = window_item_find(channel->server, channel->visible_name);

    if ( item )
    {
        SERVER_REC *server = channel->server;
        insertChat(channel->visible_name, server->tag, item, nicklist_getnicks(channel));
    }
}

/**
 * Called when emited channel destroyed signal.
 *
 * @param *channel Channel.
 */
static void channelDestroyed (CHANNEL_REC *channel)
{
    SERVER_REC *server = channel->server;

    removeChat(channel->visible_name, server->tag);
}

/**
 * Called when emited query created signal.
 *
 * @param *query Query.
 */
static void queryCreated (QUERY_REC *query)
{
    WI_ITEM_REC *item = window_item_find(query->server, query->name);

    if ( item )
    {
        SERVER_REC *server = query->server;
        insertChat(query->name, server->tag, item, NULL);
    }
}

/**
 * Called when emited query destroyed signal.
 *
 * @param *query Query.
 */
static void queryDestroyed (QUERY_REC *query)
{
    SERVER_REC *server = query->server;

    removeChat(query->name, server->tag);
}

/**
 * Called when emited query nick changed signal.
 *
 * @param *query       Query.
 * @param *oldChatName Old query name.
 */
static void queryRenamed (QUERY_REC *query, char *oldChatName)
{
    SERVER_REC *server = query->server;

    renameChat(server->tag, oldChatName, query->name);
}

/**
 * Called when emited print text signal, mostly copy paste from Irssi.
 *
 * @param *dest Destination.
 * @param *text Text.
 */
static void msg (TEXT_DEST_REC *dest, const char *text)
{
    THEME_REC *theme;
    char *dup, *str, *ptr, *nick, *line, type;
    int fgcolor, bgcolor;
    int flags;
    int hide_text_style = settings_get_bool("hide_text_style");
    int hide_colors     = hide_text_style || settings_get_bool("hide_colors");
    int hilight_color   = format_string_expand(settings_get_str("hilight_color"), NULL)[1]-'0';
    int hasNick         = 0;

    theme = window_get_theme(dest->window);

    dup  = str = g_strdup(text);
    line = NULL;
    nick = "<nick><span style='color: <foreground>;'>*</span></nick>";

    flags   = 0;
    fgcolor = theme->default_color;
    bgcolor = -1;

    while ( *str != '\0' )
    {
        type = '\0';

        for ( ptr = str; *ptr != '\0'; ptr++ )
        {
            if ( IS_COLOR_CODE(*ptr) || *ptr == '\n' )
            {
                type = *ptr;
                *ptr++ = '\0';

                break;
            }
        }

        if ( type == 4 && *ptr == FORMAT_STYLE_CLRTOEOL )
        {
            /* clear to end of line */
            flags |= GUI_PRINT_FLAG_CLRTOEOL;
        }

        if ( *str != '\0' || (flags & GUI_PRINT_FLAG_CLRTOEOL) )
        {

            line   = appendText(str, line, bgcolor, fgcolor, flags);
            flags &= ~(GUI_PRINT_FLAG_INDENT|GUI_PRINT_FLAG_CLRTOEOL);
        }

        if ( *ptr == '\0' )
        {
            break;
        }

        switch ( type )
        {
            case 2:
            {
                /* bold */
                if ( !hide_text_style )
                {
                    flags ^= GUI_PRINT_FLAG_BOLD;
                }

                break;
            }

            case 3:
            {
                /* MIRC color */
                get_mirc_color((const char **) &ptr, hide_colors ? NULL : &fgcolor,
                               hide_colors ? NULL : &bgcolor);

                if ( !hide_colors )
                {
                        flags |= GUI_PRINT_FLAG_MIRC_COLOR;
                }

                break;
            }

            case 4:
            {
                /* user specific colors */
                flags &= ~GUI_PRINT_FLAG_MIRC_COLOR;

                switch ( *ptr )
                {
                    case FORMAT_STYLE_BLINK:
                    {
                        flags ^= GUI_PRINT_FLAG_BLINK;

                        break;
                    }

                    case FORMAT_STYLE_UNDERLINE:
                    {
                        flags ^= GUI_PRINT_FLAG_UNDERLINE;

                        break;
                    }

                    case FORMAT_STYLE_BOLD:
                    {
                        flags ^= GUI_PRINT_FLAG_BOLD;

                        break;
                    }

                    case FORMAT_STYLE_REVERSE:
                    {
                        flags ^= GUI_PRINT_FLAG_REVERSE;

                        break;
                    }

                    case FORMAT_STYLE_MONOSPACE:
                    {
                        flags ^= GUI_PRINT_FLAG_MONOSPACE;

                        break;
                    }

                    case FORMAT_STYLE_INDENT:
                    {
                        flags |= GUI_PRINT_FLAG_INDENT;

                        nick    = formatNick(line);
                        line    = NULL;
                        hasNick = 1;

                        break;
                    }

                    case FORMAT_STYLE_DEFAULTS:
                    {
                        fgcolor = theme->default_color;
                        bgcolor = -1;
                        flags  &= GUI_PRINT_FLAG_INDENT|GUI_PRINT_FLAG_MONOSPACE;

                        break;
                    }

                    case FORMAT_STYLE_CLRTOEOL:
                    {
                        break;
                    }

                    default:
                    {
                        if ( *ptr != FORMAT_COLOR_NOCHANGE )
                        {
                            fgcolor = (unsigned char) *ptr-'0';

                            if ( fgcolor == hilight_color )
                            {

                                if ( !flags & GUI_PRINT_FLAG_BOLD )
                                {
                                    flags ^= GUI_PRINT_FLAG_BOLD;
                                }
                            }
                        }

                        if ( ptr[1] == '\0' )
                        {
                            break;
                        }

                        ptr++;

                        if ( *ptr != FORMAT_COLOR_NOCHANGE )
                        {
                            bgcolor = *ptr-'0';
                        }
                    }
                }

                ptr++;

                break;
            }

            case 15:
            {
                /* remove all styling */
                fgcolor = theme->default_color;
                bgcolor = -1;
                flags  &= GUI_PRINT_FLAG_INDENT|GUI_PRINT_FLAG_MONOSPACE;

                break;
            }

            case 31:
            {
                /* underline */
                if ( !hide_text_style )
                {
                    flags ^= GUI_PRINT_FLAG_UNDERLINE;
                }

                break;
            }

            case 27:
            {
                /* ansi color code */
                ptr = (char *) get_ansi_color(theme, ptr, hide_colors ? NULL : &fgcolor,
                                              hide_colors ? NULL : &bgcolor,
                                              hide_colors ? NULL : &flags);

                break;
            }
        }

        str = ptr;
    }

    if ( ownMsg == 1 )
    {
        newOwnMsg(nick, line, dest);
    }
    else if ( dest->level & MSGLEVEL_HILIGHT )
    {
        newHightlightMsg(nick, line, dest);
    }
    else
    {
        newMsg(nick, line, dest);
    }

    g_free(dup);
    g_free(line);

    if ( hasNick == 1 )
    {
        g_free(nick);
    }

    ownMsg = 0;
}

/**
 * Called when emited message own public/private signals.
 *
 * @param *dcc DCC request.
 */
static void msgOwn ()
{
    ownMsg = 1;
}

/**
 * Called when emited dcc request signal.
 *
 * @param *dcc DCC request.
 */
static void dcc (GET_DCC_REC *dcc)
{
    if ( IS_DCC_GET(dcc) )
    {
        newDccFile(dcc->nick, dcc->arg, dcc->servertag);
    }
}

/**
 * Called when emited dcc closed or dcc connected signal.
 *
 * @param *dcc DCC request.
 */
static void closeDcc (GET_DCC_REC *dcc)
{
    if ( IS_DCC_GET(dcc) )
    {
        closeDccFile(dcc->nick, dcc->arg, dcc->servertag);
    }
}

/**
 * Called when emited nicklist new signal.
 *
 * @param *channel Channel.
 * @param *nick    Nick.
 */
static void nickAdded (CHANNEL_REC *channel, NICK_REC *nick)
{
    SERVER_REC *server = channel->server;
    char *prefix       = nick->op ? "@" : nick->halfop ? "%" : nick->voice ? "+" : "";

    insertNick(server->tag, channel->visible_name, nick->nick, prefix);
}

/**
 * Called when emited nicklist remove signal.
 *
 * @param *channel Channel.
 * @param *nick    Nick.
 */
static void nickRemoved (CHANNEL_REC *channel, NICK_REC *nick)
{
    SERVER_REC *server = channel->server;
    char *prefix       = nick->op ? "@" : nick->halfop ? "%" : nick->voice ? "+" : "";

    removeNick(server->tag, channel->visible_name, nick->nick, prefix);
}

/**
 * Called when emited nicklist changed signal.
 *
 * @param *channel Channel.
 * @param *nick    Nick.
 * @param *oldNick Old nick.
 */
static void nickChanged (CHANNEL_REC *channel, NICK_REC *nick, char *oldNick)
{
    SERVER_REC *server = channel->server;
    char *prefix       = nick->op ? "@" : nick->halfop ? "%" : nick->voice ? "+" : "";

    changeNick(server->tag, channel->visible_name, nick->nick, oldNick, prefix);
}

/**
 * Called when emited nick mode changed signal.
 *
 * @param *channel Channel.
 * @param *nick    Nick.
 */
static void nickModeChanged (CHANNEL_REC *channel, NICK_REC *nick)
{
    SERVER_REC *server = channel->server;
    char *prefix       = nick->op ? "@" : nick->halfop ? "%" : nick->voice ? "+" : "";

    changeNickMode(server->tag, channel->visible_name, nick->nick, prefix);
}

/**
 * Parse mIRC color string, copy paste from Irssi.
 */
static void get_mirc_color (const char **str, int *fg_ret, int *bg_ret)
{
    int fg, bg;

    fg = fg_ret == NULL ? -1 : *fg_ret;
    bg = bg_ret == NULL ? -1 : *bg_ret;

    if ( !i_isdigit(**str) && **str != ',' )
    {
        fg = -1;
        bg = -1;
    }
    else
    {
        /* foreground color */

        if ( **str != ',' )
        {
            fg = **str-'0';

            (*str)++;

            if ( i_isdigit(**str) )
            {
                fg = fg*10 + (**str-'0');

                (*str)++;
            }
        }

        if ( **str == ',' )
        {
            /* background color */

            if ( !i_isdigit((*str)[1]) )
            {
                bg = -1;
            }
            else
            {
                (*str)++;

                bg = **str-'0';

                (*str)++;

                if ( i_isdigit(**str) )
                {
                    bg = bg*10 + (**str-'0');

                    (*str)++;
                }
            }
        }
    }

    if ( fg_ret )
    {
        *fg_ret = fg;
    }

    if ( bg_ret )
    {
        *bg_ret = bg;
    }
}

/**
 * Parse ANSI color string, copy paste from Irssi.
 *
 * @param *theme     Theme.
 * @param *str       String.
 * @param *fg_ret    Foreground color.
 * @param *bg_ret    Background color.
 * @param *flags_ret Flags.
 */
static const char *get_ansi_color (THEME_REC *theme, const char *str,
                                   int *fg_ret, int *bg_ret, int *flags_ret)
{
    static char ansitab[8] = { 0, 4, 2, 6, 1, 5, 3, 7 };
    const char *start;
    int fg, bg, flags, num;

    if ( *str != '[' )
    {
        return str;
    }

    start = str++;

    fg = fg_ret == NULL || *fg_ret < 0 ? theme->default_color : *fg_ret;
    bg = bg_ret == NULL || *bg_ret < 0 ? -1 : *bg_ret;
    flags = flags_ret == NULL ? 0 : *flags_ret;

    num = 0;
    for ( ;; str++ )
    {
        if ( *str == '\0' )
        {
            return start;
        }

        if ( i_isdigit(*str) )
        {
            num = num*10 + (*str-'0');

            continue;
        }

        if ( *str != ';' && *str != 'm' )
        {
            return start;
        }

        switch ( num )
        {
            case 0:
            {
                /* reset colors back to default */
                fg = theme->default_color;
                bg = -1;
                //flags &= ~GUI_PRINT_FLAG_INDENT;

                break;
            }

            case 1:
            {
                /* hilight */
                flags |= GUI_PRINT_FLAG_BOLD;

                break;
            }
            case 5:
            {
                /* blink */
                //flags |= GUI_PRINT_FLAG_BLINK;

                break;
            }

            case 7:
            {
                /* reverse */
                //flags |= GUI_PRINT_FLAG_REVERSE;

                break;
            }

            default:
            {
                if ( num >= 30 && num <= 37 )
                {
                    if ( fg == -1 )
                    {
                        fg = 0;
                    }

                    fg = (fg & 0xf8) | ansitab[num-30];
                }
                if ( num >= 40 && num <= 47 )
                {
                    if ( bg == -1 )
                    {
                        bg = 0;
                    }

                    bg = (bg & 0xf8) | ansitab[num-40];
                }

                break;
            }
        }

        num = 0;

        if ( *str == 'm' )
        {
            if ( fg_ret != NULL )
            {
                *fg_ret = fg;
            }

            if ( bg_ret != NULL )
            {
                *bg_ret = bg;
            }

            if ( flags_ret != NULL )
            {
                *flags_ret = flags;
            }

            str++;

            break;
        }
    }

    return str;
}

/**
 * Used by completeIrssi function, copy paste from Irssi.
 */
static void free_completions ()
{
    complist = g_list_first(complist);

    g_list_foreach(complist, (GFunc) g_free, NULL);
    g_list_free(complist);
    complist = NULL;

    g_free_and_null(last_line);
}

/**
 * Used by completeIrssi function, copy paste from Irssi.
 */
static char *get_word_at (const char *str, int pos, char **startpos)
{
    const char *start, *end;

    g_return_val_if_fail(str != NULL, NULL);
    g_return_val_if_fail(pos >= 0, NULL);

    /* get previous word if char at `pos' is space */
    start = str+pos;
    while ( start > str && isseparator(start[-1]) )
    {
        start--;
    }

    end = start;

    while ( start > str && !isseparator(start[-1]) )
    {
        start--;
    }

    while ( *end != '\0' && !isseparator(*end) )
    {
        end++;
    }

    while ( *end != '\0' && isseparator_notspace(*end) )
    {
        end++;
    }

    *startpos = (char *) start;

    return g_strndup(start, (int) (end-start));
}
