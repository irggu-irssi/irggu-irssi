/****************************************************************************
**  irggu_irssi.h
**
**  Copyright information
**
**      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRGGU_IRSSI_H
#define IRGGU_IRSSI_H

/* FIXME: bug */
#define UOFF_T_LONG_LONG 1

/* #include "config.h" */
#include "core.h"
#include "common.h"
#include "modules.h"
#include "commands.h"
#include "settings.h"
#include "printtext.h"
#include "window-items.h"
#include "window-activity.h"
#include "levels.h"
#include "servers.h"
#include "chat-protocols.h"
#include "channels.h"
#include "queries.h"
#include "nicklist.h"
#include "chatnets.h"
#include "servers-reconnect.h"
#include "masks.h"
#include "misc.h"
#include "rawlog.h"
#include "log.h"
#include "ignore.h"
#include "fe-exec.h"
#include "pidwait.h"
#include "irc/core/irc.h"
#include "dcc/dcc-get.h"
#include "module.h"
#include "module-formats.h"
#include "fe-windows.h"

#endif // IRGGU_IRSSI_H
