/****************************************************************************
**  irggu_core.c
**
**  Copyright information
**
**      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "irggu_core.h"

/**
 * ABI version check.
 */
#ifdef IRSSI_ABI_VERSION
void irggu_abicheck (int *version)
{
    *version = IRSSI_ABI_VERSION;
}
#endif

/**
 * Init module.
 */
void irggu_init ()
{
    module_register(MODULE, "core");
    handlerIrssiInit();
}

/**
 * Deinit module.
 */
void irggu_deinit ()
{
    handlerIrssiDeinit();
}

