/****************************************************************************
**  handler_server.h
**
**  Copyright information
**
**      Copyright (C) 2011-2013 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Irssi.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef HANDLER_SERVER_H
#define HANDLER_SERVER_H

#define UOFF_T_LONG_LONG 1

#include "common.h"
#include "fe-windows.h"
#include "nicklist.h"
#include "printtext.h"
#include <QMap>
#include <QString>

WINDOW_REC *statusWindow;
QMap<QString, SERVER_REC *> serversList;
QMap<QString, QMap<QString, WI_ITEM_REC *> > chatsList;

void writeSettings();

extern "C" void init();
extern "C" void deinit();
extern "C" void connectToServer();
extern "C" void disconnect();
extern "C" void registerToServer(const char *data, int force);
extern "C" void setSetting(const char *data);
extern "C" void insertStatus(WINDOW_REC *win);
extern "C" void insertServer(char *name, SERVER_REC *server);
extern "C" void insertChat(char *name, char *server, WI_ITEM_REC *item, GSList *nicks);
extern "C" void insertNick(char *server, char *chat, char *nick, char *prefix);
extern "C" void removeServer(char *name);
extern "C" void removeChat(char *name, char *server);
extern "C" void removeNick(char *server, char *chat, char *nick, char *prefix);
extern "C" void changeNick(char *server, char *chat, char *nick, char *oldNick, char *prefix);
extern "C" void changeNickMode(char *server, char *chat, char *nick, char *prefix);
extern "C" void renameChat(char *server, char *oldChatName, char *newChatName);
extern "C" void newMsg(char *nick, char *msg, TEXT_DEST_REC *dest);
extern "C" void newHightlightMsg(char *nick, char *msg, TEXT_DEST_REC *dest);
extern "C" void newOwnMsg(char *nick, char *msg, TEXT_DEST_REC *dest);
extern "C" void newDccFile(char *nick, char *file, char *server);
extern "C" void closeDccFile(char *nick, char *file, char *server);

#endif // HANDLER_SERVER_H
