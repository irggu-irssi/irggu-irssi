#-------------------------------------------------
#
# Project created by QtCreator 2011-04-09T02:40:42
#
#-------------------------------------------------

QT       += core \
    network
QT       -= gui

TARGET = irggu
TEMPLATE = lib


SOURCES += \
    irggu_core.c \
    handler_irssi.c \
    handler_server.cpp \
    irggu_misc.cpp \
    network/localsocket.cpp \
    connection.cpp

HEADERS +=\
    irggu_irssi.h \
    irggu_core.h \
    handler_irssi.h \
    handler_server.h \
    irggu_misc.h \
    protocol/irggu.h \
    network/localsocket.h \
    connection.h

INCLUDEPATH += /usr/include/irssi/src/ \
    /usr/include/irssi/src/core/ \
    /usr/include/irssi/src/fe-common/ \
    /usr/include/irssi/src/fe-common/core/ \
    /usr/include/irssi/src/fe-text/ \
    /usr/include/irssi/src/irc/ \
    /usr/include/irssi/src/lib-config

CONFIG += link_pkgconfig \
    libtool
PKGCONFIG += glib-2.0

DEFINES += MODULE=\\\"irggu\\\" \
    SOCK_FILE=\\\"/tmp/irggu.sock\\\"

OTHER_FILES += \
    CMakeLists.txt
